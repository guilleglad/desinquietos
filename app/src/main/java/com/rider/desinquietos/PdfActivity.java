package com.rider.desinquietos;

import android.content.Context;
import android.content.Intent;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;

import com.facebook.drawee.view.SimpleDraweeView;
import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import es.voghdev.pdfviewpager.library.PDFViewPager;
import es.voghdev.pdfviewpager.library.adapter.BasePDFPagerAdapter;
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter;
import es.voghdev.pdfviewpager.library.asset.CopyAsset;
import es.voghdev.pdfviewpager.library.asset.CopyAssetThreadImpl;

public class PdfActivity extends AppCompatActivity {
    PDFViewPager pdfViewPager;
    BasePDFPagerAdapter adapter;
    SimpleDraweeView btn_profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        pdfViewPager = (PDFViewPager)findViewById(R.id.pdfViewPager);
        adapter = new PDFPagerAdapter(getApplicationContext(), "bases.pdf");
        pdfViewPager.setAdapter(adapter);
        btn_profile = (SimpleDraweeView)findViewById(R.id.btn_profile);
        btn_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(btn_profile)
                        .scale((float)0.8,1)
                        .duration(500)
                        .descelerate()
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        if (LoginActivity.isLogged_in()) {
                            Intent I = new Intent(getApplicationContext(), FichaActivity.class);
                            startActivity(I);
                        } else {
                            Intent I = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(I);
                            finish();
                        }
                    }
                });
            }
        });
        btn_profile.setImageURI(Uri.parse(LoginActivity.getFoto()));

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (adapter != null) {
        adapter.close();
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return false;
    }


}
