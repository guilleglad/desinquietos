package com.rider.desinquietos;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.MenuItem;
import android.support.v4.app.NavUtils;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.github.florent37.viewanimator.ViewAnimator;
import com.rider.desinquietos.misc.Constants;


public class CarnetActivity extends AppCompatActivity {

    private ImageView img_carnet;
    private Bundle extras;

    @Override
    protected void onResume() {
        super.onResume();
        FrameLayout _fl = (FrameLayout)findViewById(R.id.id_layout_carnet);
        _fl.setRotationX(-90);
        ViewAnimator
                .animate(_fl)
                .rotationX(0)
                .duration(500)
                .descelerate()
                .start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        extras = getIntent().getExtras();
        setContentView(R.layout.activity_carnet);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        img_carnet = (ImageView)findViewById(R.id.img_carnet);
        draw_over(img_carnet);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button.
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void draw_over(ImageView img_carnet){
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inMutable=true;
        Bitmap bmp = BitmapFactory.decodeResource(getResources(),R.mipmap.carne_x,bitmapOptions);
        Canvas C =new Canvas(bmp);
        Paint P = createPaint();
        draw_nombre(C,bmp,P,img_carnet);
        draw_DNI(C,bmp,P,img_carnet);
        draw_fechanac(C,bmp,P,img_carnet);
        draw_validez(C,bmp,P,img_carnet);
    }
    public Paint createPaint(){
        Paint P = new Paint();
        P.setColor(Color.WHITE);
        int mGestureThreshold = getResources().getDimensionPixelSize(R.dimen.text_carnet);
        P.setTextSize(mGestureThreshold);
        P.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(),
                "fonts/Roboto-Regular.ttf"));
        return P;
    }
    public void draw_nombre(Canvas C,Bitmap bmp,Paint P,ImageView img){
        int _height = bmp.getHeight();
        int _width = bmp.getWidth();
        int _posx = 5*_width/100;
        int _posy = 57*_height/100;
        C.drawText(extras.getString("Nombre"),_posx,_posy,P);
        img.setImageBitmap(bmp);
        img.invalidate();
    }
    public void draw_DNI(Canvas C,Bitmap bmp,Paint P,ImageView img){
        int _height = bmp.getHeight();
        int _width = bmp.getWidth();
        int _posx = 5*_width/100;
        int _posy = 75*_height/100;
        C.drawText(extras.getString("NIF"),_posx,_posy,P);
        img.setImageBitmap(bmp);
        img.invalidate();
    }
    public void draw_fechanac(Canvas C,Bitmap bmp,Paint P,ImageView img){
        int _height = bmp.getHeight();
        int _width = bmp.getWidth();
        int _posx = 72*_width/100;
        int _posy = 57*_height/100;
        C.drawText(extras.getString("FechaNac"),_posx,_posy,P);
        img.setImageBitmap(bmp);
        img.invalidate();
    }
    public void draw_validez(Canvas C,Bitmap bmp,Paint P,ImageView img){
        int _height = bmp.getHeight();
        int _width = bmp.getWidth();
        int _posx = 72*_width/100;
        int _posy = 75*_height/100;
        C.drawText(extras.getString("Validez"),_posx,_posy,P);
        img.setImageBitmap(bmp);
        img.invalidate();
    }
}
