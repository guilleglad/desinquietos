package com.rider.desinquietos;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;
import com.rider.desinquietos.custom.AppController;
import com.rider.desinquietos.custom.VolleyMultipartRequest;
import com.rider.desinquietos.misc.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class FichaActivity extends AppCompatActivity {

    private static final String CAPTURE_IMAGE_FILE_PROVIDER = "com.rider.desinquietos.fileprovider";
    private static final int IMAGE_REQUEST_CODE = 1;
    private static final int REQUEST_IMAGE_GALLERY = 2;
    private ImageView btn_refresh;
    private StringRequest profileRequest;
    private SimpleDraweeView img_loader;
    private Uri _uri;
    private ImageView img_carnet;
    private Button btn_baseslegales;
    private String Nombre,NIF,FechaNac,Validez;
    private String img_name = "image_perfil.jpg";
    private Button btn_guardar;
    private ControllerListener _cListener;
    private ImageView img_dummy;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ficha);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.indicator), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        _cListener = new BaseControllerListener<ImageInfo>(){
            @Override
            public void onSubmit(String id, Object callerContext) {
                ((ProgressBar)findViewById(R.id.progress_image)).setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                ((ProgressBar)findViewById(R.id.progress_image)).setVisibility(View.INVISIBLE);

            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                Toast.makeText(FichaActivity.this, getResources().getString(R.string.msg_sin_foto), Toast.LENGTH_LONG).show();
                ((ProgressBar)findViewById(R.id.progress_image)).setVisibility(View.INVISIBLE);
            }
        };
        btn_guardar = (Button)findViewById(R.id.btn_guardar);
        btn_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(btn_guardar)
                        .scale((float)0.8,1)
                        .descelerate()
                        .duration(500)
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        guardar_foto();
                    }
                });
            }
        });
        profileRequest = new StringRequest(Request.Method.GET, Constants.url + "/" + Constants.url_usuario + "?nif=" + LoginActivity.getNIF(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jres = new JSONObject(response);
                    ((TextInputEditText)findViewById(R.id.edit_nombres_apellidos)).setText(jres.getString("Nombre"));
                    Nombre = jres.getString("Nombre");
                    String strCurrentDate = jres.getString("Fecha_Nacimiento");
                    strCurrentDate = strCurrentDate.replace("T"," ");
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
                    Date newDate = format.parse(strCurrentDate);
                    format = new SimpleDateFormat("dd/MM/yyyy");
                    ((TextInputEditText)findViewById(R.id.edit_fecha_nac)).setText(format.format(newDate));
                    FechaNac = format.format(newDate);
                    ((TextInputEditText)findViewById(R.id.edit_telefono)).setText(jres.getString("Telefono"));
                    ((TextInputEditText)findViewById(R.id.edit_dni)).setText(jres.getString("NIF"));
                    NIF = jres.getString("NIF");
                    strCurrentDate = jres.getString("Fecha_Validez");
                    strCurrentDate = strCurrentDate.replace("T"," ");
                    format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
                    newDate = format.parse(strCurrentDate);
                    format = new SimpleDateFormat("dd/MM/yyyy");
                    ((TextInputEditText)findViewById(R.id.edit_validez)).setText(format.format(newDate));
                    Validez = format.format(newDate);
                    ((TextInputEditText)findViewById(R.id.edit_email)).setText(jres.getString("Email"));
//                    ((TextInputEditText)findViewById(R.id.edit_favoritos)).setText(jres.getString(""));
                    ((TextInputEditText)findViewById(R.id.edit_favoritos)).setText("-");

                    img_loader = (SimpleDraweeView)findViewById(R.id.foto_loader);
                    _uri = Uri.parse(jres.getString("Foto"));
                    if(_uri != null){
                        DraweeController _myController = Fresco.newDraweeControllerBuilder()
                                .setControllerListener(_cListener)
                                .setUri(_uri)
                                .build();
                        img_loader.setController(_myController);
                        img_loader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(((ProgressBar)findViewById(R.id.progress_image)).getVisibility() == View.INVISIBLE){
                                    ViewAnimator
                                            .animate(img_loader)
                                            .scale((float)0.8,1)
                                            .descelerate()
                                            .duration(500)
                                            .start().onStop(new AnimationListener.Stop() {
                                        @Override
                                        public void onStop() {
                                            openChooserDialog();
                                        }
                                    });
                                }else{
                                    Toast.makeText(FichaActivity.this, getResources().getString(R.string.msg_foto_cargando), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }

                    img_carnet.setVisibility(View.VISIBLE);
                    btn_baseslegales = (Button)findViewById(R.id.btn_baseslegales);
                    btn_baseslegales.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ViewAnimator
                                .animate(btn_baseslegales)
                                .scale((float)0.8,1)
                                .descelerate()
                                .duration(500)
                                .start().onStop(new AnimationListener.Stop() {
                                @Override
                                public void onStop() {
                                    Intent I = new Intent(getApplicationContext(),BasesActivity.class);
                                    startActivity(I);
                                }
                            });
                        }
                    });
                    btn_baseslegales.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        addRequest();
        img_dummy = new ImageView(getApplicationContext());
        btn_refresh = (ImageView)findViewById(R.id.btn_refresh);
        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((ProgressBar)findViewById(R.id.progress_image)).getVisibility() == View.INVISIBLE){
                    ImagePipeline _ip = Fresco.getImagePipeline();
                    if(_uri != null )
                        _ip.evictFromCache(_uri);
                    addRequest();
                }else{
                    Toast.makeText(FichaActivity.this, getResources().getString(R.string.msg_foto_cargando), Toast.LENGTH_SHORT).show();
                }
            }
        });
        img_carnet = (ImageView)findViewById(R.id.img_carnet);
        img_carnet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(img_carnet)
                        .scale((float)0.8,1)
                        .descelerate()
                        .duration(500)
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        Intent I = new Intent(getApplicationContext(),CarnetActivity.class);
                        I.putExtra("Nombre",Nombre);
                        I.putExtra("NIF",NIF);
                        I.putExtra("FechaNac",FechaNac);
                        I.putExtra("Validez",Validez);
                        startActivity(I);
                    }
                });
            }
        });

    }

    private void guardar_foto() {
        if(img_loader.getDrawable() == null) {
            Snackbar.make(img_loader, getResources().getString(R.string.err_img_loader), Snackbar.LENGTH_LONG).show();
            return;
        }
        String url = Constants.url + "/" +Constants.url_subirfoto+"?nif="+LoginActivity.getNIF();
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String res = "";
                try {
                    res = new String(response.data,"UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                Log.i("RESPONSE:",res);
                if(res.contains("nif="+LoginActivity.getNIF())){
                    ((ProgressBar)findViewById(R.id.progress_image)).setVisibility(View.INVISIBLE);
                    btn_guardar.setText(getResources().getString(R.string.msg_guardar_img));
                    btn_guardar.setEnabled(true);
                    btn_guardar.setVisibility(View.GONE);
                    Toast.makeText(FichaActivity.this, getResources().getString(R.string.msg_img_guardada), Toast.LENGTH_SHORT).show();
                    ImagePipeline _ip = Fresco.getImagePipeline();
                    if(_uri != null )
                        _ip.evictFromCache(_uri);
                    addRequest();
                }else{
                    Toast.makeText(FichaActivity.this, getResources().getString(R.string.err_upload_img), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(FichaActivity.this, getResources().getString(R.string.err_upload_img), Toast.LENGTH_SHORT).show();
                ((ProgressBar)findViewById(R.id.progress_image)).setVisibility(View.INVISIBLE);
                btn_guardar.setText(getResources().getString(R.string.msg_guardar_img));
                btn_guardar.setEnabled(true);
                btn_guardar.setVisibility(View.GONE);
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

//                        Log.e("Error Status", status);
//                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message+" Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message+ " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message+" Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
//                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                params.put("foto", new DataPart(img_name, getFileDataFromDrawable(getBaseContext(), img_dummy.getDrawable()), "image/jpeg"));
                return params;
            }
        };
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(0,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(multipartRequest,"UPLOAD_PIC");
        ((ProgressBar)findViewById(R.id.progress_image)).setVisibility(View.VISIBLE);
        btn_guardar.setText(getResources().getString(R.string.msg_guardando));
        btn_guardar.setEnabled(false);
    }
    public static byte[] getFileDataFromDrawable(Context context, Drawable drawable) {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    public void addRequest(){
        profileRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(profileRequest,"Profile");
    }
    @Override
    public void onBackPressed() {
        finish();
    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return false;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IMAGE_REQUEST_CODE) {
            if (resultCode == this.RESULT_OK) {
                File path = new File(getFilesDir(), "/");
                if (!path.exists())
                    path.mkdirs();
                File imageFile = new File(path, img_name);
                if(imageFile!=null){
                    ImageRequest imageRequestBuilder = ImageRequestBuilder.newBuilderWithSource(Uri.fromFile(imageFile))
                            .setAutoRotateEnabled(true)
                            .setResizeOptions(new ResizeOptions(img_loader.getLayoutParams().width, img_loader.getLayoutParams().height))
                            .build();
                    DraweeController _myController = Fresco.newDraweeControllerBuilder()
                            .setImageRequest(imageRequestBuilder)
                            .setControllerListener(_cListener)
                            .build();
                    img_loader.setController(_myController);
                    ImagePipeline imagePipeline = Fresco.getImagePipeline();
                    DataSource<CloseableReference<CloseableImage>> dataSource =
                            imagePipeline.fetchDecodedImage(imageRequestBuilder,getApplicationContext());
                    dataSource.subscribe(new BaseBitmapDataSubscriber() {

                        @Override
                        public void onNewResultImpl(@Nullable Bitmap bitmap) {
                            // You can use the bitmap in only limited ways
                            // No need to do any cleanup.
                            img_dummy.setImageBitmap(bitmap);
                        }

                        @Override
                        public void onFailureImpl(DataSource dataSource) {
                            // No cleanup required here.
                        }

                    }, CallerThreadExecutor.getInstance());
                    //img_dummy.setImageURI(Uri.fromFile(imageFile));
                    btn_guardar.setVisibility(View.VISIBLE);
                }
            }
        }
        if (requestCode == REQUEST_IMAGE_GALLERY){
            if(resultCode == this.RESULT_OK){
                Uri selectedImage = data.getData();
                ImageRequest imageRequestBuilder = ImageRequestBuilder.newBuilderWithSource(selectedImage)
                        .setAutoRotateEnabled(true)
                        .setResizeOptions(new ResizeOptions(img_loader.getLayoutParams().width, img_loader.getLayoutParams().height))
                        .build();
                DraweeController _myController = Fresco.newDraweeControllerBuilder()
                        .setImageRequest(imageRequestBuilder)
                        .setControllerListener(_cListener)
                        .build();
                img_loader.setController(_myController);
                ImagePipeline imagePipeline = Fresco.getImagePipeline();
                DataSource<CloseableReference<CloseableImage>> dataSource =
                        imagePipeline.fetchDecodedImage(imageRequestBuilder,getApplicationContext());
                dataSource.subscribe(new BaseBitmapDataSubscriber() {

                    @Override
                    public void onNewResultImpl(@Nullable Bitmap bitmap) {
                        // You can use the bitmap in only limited ways
                        // No need to do any cleanup.
                        img_dummy.setImageBitmap(bitmap);
                    }

                    @Override
                    public void onFailureImpl(DataSource dataSource) {
                        // No cleanup required here.
                    }

                }, CallerThreadExecutor.getInstance());
                //img_dummy.setImageURI(selectedImage);
                btn_guardar.setVisibility(View.VISIBLE);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    public void openChooserDialog(){
        String[] addPhoto;
        addPhoto = new String[] {
            getResources().getString(R.string.camara),
            getResources().getString(R.string.galeria)
        };
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getResources().getString(R.string.camera_dialog_title));

        dialog.setItems(addPhoto, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

                if (id == 0) {
                    File path = new File(getFilesDir(), "/");
                    if (!path.exists()) path.mkdirs();
                    File image = new File(path, img_name);
                    Uri imageUri = FileProvider.getUriForFile(FichaActivity.this, CAPTURE_IMAGE_FILE_PROVIDER, image);
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                    startActivityForResult(intent, IMAGE_REQUEST_CODE);
                }
                if (id == 1) {
                    Intent pickPhoto = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, REQUEST_IMAGE_GALLERY);
                }
            }
        });

        dialog.setNeutralButton(getResources().getString(R.string.cancel),
                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        dialog.show();

    }
}
