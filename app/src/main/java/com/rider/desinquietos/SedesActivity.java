package com.rider.desinquietos;

import android.Manifest;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.ArrayMap;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rider.desinquietos.custom.AppController;
import com.rider.desinquietos.misc.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

public class SedesActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener,GoogleMap.OnMarkerClickListener {

    private static final String TAG_LOCATIONCHANGED = "LOCATION CHANGED";
    private static final String TAG_LOCATIONSERVICES = "LOCATION SERVICES";
    private Bundle extras;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 1357;
    private TextView text_subtitle;
    private Double last_latitude;
    private Double last_longitud;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        extras = getIntent().getExtras();
        setContentView(R.layout.activity_sedes2);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //TOOLBAR
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.indicator), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        //TOOLBAR*

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mLocationRequest = LocationRequest
                .create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(Constants.UPDATE_INTERVAL_IN_MILLISECONDS)
                .setFastestInterval(
                        Constants.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode){
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                } else {
                    // Permission Denied
//                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg_location_permiso), Toast.LENGTH_LONG)
//                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if(Build.VERSION.SDK_INT >= 23){
                requestPermissions(new String[] {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                        REQUEST_CODE_ASK_PERMISSIONS);
            }
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.getUiSettings().setTiltGesturesEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.setTrafficEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setIndoorEnabled(true);
        mMap.setBuildingsEnabled(false);
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter(){

            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                LinearLayout info = new LinearLayout(SedesActivity.this);
                info.setOrientation(LinearLayout.VERTICAL);

                TextView title = new TextView(SedesActivity.this);
                title.setTextColor(Color.BLACK);
                title.setGravity(Gravity.CENTER);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());

                TextView snippet = new TextView(SedesActivity.this);
                snippet.setTextColor(Color.GRAY);
                snippet.setGravity(Gravity.CENTER);
                snippet.setText(marker.getSnippet());

                info.addView(title);
                info.addView(snippet);
                return info;
            }
        });


        mMap.moveCamera(CameraUpdateFactory.zoomTo(Constants.MAP_ZOOM));
        String p = "?oferta="+extras.getString("oferta");
        StringRequest requestSedes = new StringRequest(Request.Method.GET, Constants.url + "/"+Constants.url_sedes+p, new Response.Listener<String>() {
            ArrayList<MarkerOptions> markers = new ArrayList<>();

            @Override
            public void onResponse(String response) {
                LatLngBounds.Builder builder = null;
                Log.i("RESPONSE",response);
                if(!response.isEmpty()){
                    try {
                        JSONObject jres = new JSONObject(response);
                        JSONArray jarr = new JSONArray(jres.getString("Sedes"));
                        builder = new LatLngBounds.Builder();
                        if(jarr.length()==0) {
                            Toast.makeText(SedesActivity.this, getResources().getString(R.string.err_localizaciones), Toast.LENGTH_LONG).show();
                            return;
                        }
                        mMap.clear();
                        for(int i = 0;i<jarr.length();i++) {
                            JSONObject jobj = new JSONObject(jarr.getString(i));
                            LatLng mpos = new LatLng(Double.parseDouble(jobj.getString("latitud")), Double.parseDouble(jobj.getString("longitud")));
                            builder.include(mpos);
                            MarkerOptions mopt = new MarkerOptions();
                            mopt.position(mpos);
                            mopt.title(extras.getString("establecimiento")+" "+jobj.getString("nombre"));
                            mopt.snippet(jobj.getString("direccion"));
                            mMap.addMarker(mopt);
                            markers.add(mopt);

                        }
                        LatLngBounds bounds = builder.build();
                        int padding = Constants.map_padding_localizaciones; // offset from edges of the map in pixels
                        CameraUpdate cu;
                        if(markers.size()==1) {
                            cu = CameraUpdateFactory.newLatLng(markers.get(0).getPosition());
                        }else{
                            cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                        }
                        mMap.moveCamera(cu);
                        mMap.animateCamera(cu);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestSedes.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(requestSedes);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG_LOCATIONSERVICES, "Location services connected.");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if(Build.VERSION.SDK_INT >= 23){
                requestPermissions(new String[] {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                        REQUEST_CODE_ASK_PERMISSIONS);
            }
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//        if (location == null) {
//            LocationServices.FusedLocationApi.requestLocationUpdates(
//                    mGoogleApiClient, mLocationRequest, this);
//        } else {
//            handleNewLocation(location);
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG_LOCATIONSERVICES, "Location services suspended. Please reconnect.");
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        if (result.hasResolution()) {
            try {
                result.startResolutionForResult(this,
                        Constants.CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG_LOCATIONSERVICES, "Location services connection failed with code "
                    + result.getErrorCode());
        }
    }

    public void handleNewLocation(Location location){
        Log.d(TAG_LOCATIONCHANGED, location.toString());
        final double currentLatitude = location.getLatitude();
        final double currentLongitude = location.getLongitude();
        this.last_latitude = currentLatitude;
        this.last_longitud = currentLongitude;
        final LatLng _location = new LatLng(currentLatitude, currentLongitude);
        ArrayList<MarkerOptions> markers = new ArrayList<>();
        mMap.setOnMarkerClickListener(this);
        MarkerOptions marker_opt = new MarkerOptions();
        marker_opt.position(_location);
        marker_opt.title("Tu posición");
        markers.add(marker_opt);
        mMap.clear();
        //mMap.addMarker(marker_opt);
        for (MarkerOptions m: markers) {
            mMap.addMarker(m);
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLng(_location));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(Constants.MAP_ZOOM));
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }
    @Override
    public void onBackPressed() {
        finish();
    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return false;
    }
}
