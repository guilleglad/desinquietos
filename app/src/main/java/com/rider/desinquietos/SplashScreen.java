package com.rider.desinquietos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;


import com.github.florent37.viewanimator.ViewAnimator;
import com.rider.desinquietos.misc.Constants;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreen extends AppCompatActivity {
    private ImageView logo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        logo = (ImageView)findViewById(R.id.image_logo);
        ViewAnimator
                .animate(logo)
                .alpha(0,1)
                .scale(5,1)
                .duration(1000)
                .start();
        TimerTask TT = new TimerTask(){

            @Override
            public void run() {
                Intent I =  new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(I);
                finish();
            }
        };
        Timer T = new Timer();
        T.schedule(TT, Constants.SPLASH_SCREEN_DELAY);
    }

}
