package com.rider.desinquietos.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by Guillermo García on 07-02-2016.
 */
public class MyTextViewLite extends android.widget.TextView {

    public MyTextViewLite(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyTextViewLite(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextViewLite(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Roboto-Light.ttf");
        setTypeface(tf);
    }
}


