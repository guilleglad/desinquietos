package com.rider.desinquietos.custom;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.facebook.drawee.backends.pipeline.Fresco;


/**
 * Created by Guillermo García on 20-01-2016.
 */
public class AppController extends Application {
    public static final String TAG = AppController.class.getSimpleName();
    public static String id_login;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private static AppController mInstace;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstace = this;
        Fresco.initialize(this);
    }

    public static synchronized AppController getInstance(){
        return mInstace;
    }

    public RequestQueue getRequestQueue(){
        if(mRequestQueue == null){
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());

        }
        return mRequestQueue;
    }

    public ImageLoader getImageLoader(){
        getRequestQueue();
        if(mImageLoader == null){
            mImageLoader = new ImageLoader(this.mRequestQueue,new com.rider.desinquietos.custom.LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag){
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req){
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag){
        if(mRequestQueue != null){
            mRequestQueue.cancelAll(tag);
        }

    }
}
