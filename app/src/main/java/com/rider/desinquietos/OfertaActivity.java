package com.rider.desinquietos;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.drawee.view.SimpleDraweeView;
import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rider.desinquietos.custom.AppController;
import com.rider.desinquietos.custom.MyTextView;
import com.rider.desinquietos.custom.MyTextViewBold;
import com.rider.desinquietos.custom.MyTextViewLite;
import com.rider.desinquietos.misc.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OfertaActivity extends AppCompatActivity {

    private MyTextView text_title;
    private MyTextView text_subtitle;
    private SimpleDraweeView nv;
    private MyTextView text_beneficio;
    private MyTextViewLite text_descripcion;
    private SimpleDraweeView logo;
    private MyTextViewBold text_movil;
    private MyTextViewBold text_telefono;
    private MyTextViewBold text_email;
    private MyTextViewBold text_web;
    private ScrollView ScrollView1;
    private ImageView btn_share;
    private String url_compartir;
    private SimpleDraweeView btn_profile;
    private MyTextViewBold text_logo;
    private Button btn_localizaciones;
    private String oferta_id;
    private boolean hayLocalizaciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oferta);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //TOOLBAR
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.indicator), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        //TOOLBAR*

        init_fields();

        Bundle extras = getIntent().getExtras();
        if(extras != null)
            init_values(extras);
        checkLocalizaciones();
        //ScrollView1.setFocusableInTouchMode(true);
        //ScrollView1.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

    }

    private void checkLocalizaciones() {
        StringRequest requestSedes = new StringRequest(Request.Method.GET, Constants.url + "/"+Constants.url_sedes+"?oferta="+oferta_id, new Response.Listener<String>() {
            ArrayList<MarkerOptions> markers = new ArrayList<>();

            @Override
            public void onResponse(String response) {
                LatLngBounds.Builder builder = null;
                Log.i("RESPONSE",response);
                if(!response.isEmpty()){
                    try {
                        JSONObject jres = new JSONObject(response);
                        JSONArray jarr = new JSONArray(jres.getString("Sedes"));
                        builder = new LatLngBounds.Builder();
                        if(jarr.length()==0) {
                            hayLocalizaciones = false;
                        }else{
                            hayLocalizaciones = true;
                        }
                        btn_localizaciones.setEnabled(true);
                        ((ProgressBar)findViewById(R.id.pb_loc)).setVisibility(View.INVISIBLE);
                        return;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestSedes.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(requestSedes);
    }

    private void init_fields() {
        text_title = (MyTextView)findViewById(R.id.text_titulo);
        text_subtitle = (MyTextView)findViewById(R.id.text_subtitulo);
        nv = (SimpleDraweeView)findViewById(R.id.foto);
        text_beneficio = (MyTextView)findViewById(R.id.text_beneficio);
        text_descripcion = (MyTextViewLite)findViewById(R.id.text_descripcion);
        logo = (SimpleDraweeView)findViewById(R.id.logo);
        text_movil = (MyTextViewBold)findViewById(R.id.text_movil);
        text_logo = (MyTextViewBold)findViewById(R.id.text_logo);
        text_telefono = (MyTextViewBold)findViewById(R.id.text_telefono);
        text_email = (MyTextViewBold)findViewById(R.id.text_email);
        text_web = (MyTextViewBold)findViewById(R.id.text_web);
        btn_share = (ImageView)findViewById(R.id.btn_share);
        btn_profile = (SimpleDraweeView)findViewById(R.id.btn_profile);
        btn_localizaciones = (Button)findViewById(R.id.btn_localizaciones);

    }

    private void init_values(Bundle extras) {
        oferta_id = extras.getString("Id_oferta");
        url_compartir = extras.getString("Compartir");
        text_title.setText(extras.getString("Establecimiento"));
        text_subtitle.setText(extras.getString("Isla"));
        nv.setImageURI(Uri.parse(extras.getString("Foto")));
        text_beneficio.setText(extras.getString("Beneficio"));
        text_descripcion.setText(extras.getString("Descripcion"));
        logo.setImageURI(Uri.parse(extras.getString("Logo")));
        text_logo.setText(extras.getString("Establecimiento"));
        if(!extras.getString("Telefono_movil").isEmpty() && !extras.getString("Telefono_movil").equals("null")){
            text_movil.setText(extras.getString("Telefono_movil"));
            text_movil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewAnimator
                            .animate(text_movil)
                            .scale((float)0.8,1)
                            .descelerate()
                            .duration(500)
                            .start().onStop(new AnimationListener.Stop() {
                        @Override
                        public void onStop() {
                            Intent I = new Intent(Intent.ACTION_DIAL);
                            String uri = String.format("tel:"+ text_movil.getText().toString().trim());
                            I.setData(Uri.parse(uri));
                            startActivity(I);
                        }
                    });
                }
            });
        }

        if(!extras.getString("Telefono_fijo").isEmpty() && !extras.getString("Telefono_fijo").equals("null")){
            text_telefono.setText(extras.getString("Telefono_fijo"));
            text_telefono.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewAnimator
                            .animate(text_telefono)
                            .scale((float)0.8,1)
                            .descelerate()
                            .duration(500)
                            .start().onStop(new AnimationListener.Stop() {
                        @Override
                        public void onStop() {
                            Intent I = new Intent(Intent.ACTION_DIAL);
                            String uri = String.format("tel:" + text_telefono.getText().toString().trim());
                            I.setData(Uri.parse(uri));
                            startActivity(I);
                        }
                    });
                }
            });
        }
        text_email.setText(extras.getString("Email"));
        text_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(text_email)
                        .scale((float)0.8,1)
                        .descelerate()
                        .duration(500)
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("message/rfc822");
                        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{Constants.email});
                        i.putExtra(Intent.EXTRA_SUBJECT, "Contacto Desinquietos");
                        i.putExtra(Intent.EXTRA_TEXT   , "");
                        try {
                            startActivity(Intent.createChooser(i, "Enviar Correo..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(OfertaActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
        text_web.setText(extras.getString("Web"));
//        text_web.setText("http://www.desinquietos.com");
        text_web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(text_web)
                        .scale((float)0.8,1)
                        .duration(500)
                        .descelerate()
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        Intent I = new Intent(Intent.ACTION_VIEW);
                        I.setData(Uri.parse(text_web.getText().toString()));
                        startActivity(I);
                    }
                });
            }
        });
        text_title.requestFocus();
        btn_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(btn_profile)
                        .scale((float)0.8,1)
                        .duration(500)
                        .descelerate()
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        if (LoginActivity.isLogged_in()) {
                            Intent I = new Intent(getApplicationContext(), FichaActivity.class);
                            startActivity(I);
                        } else {
                            Intent I = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(I);
                            finish();
                        }
                    }
                });
            }
        });
        btn_profile.setImageURI(Uri.parse(LoginActivity.getFoto()));
        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(btn_share)
                        .scale((float)0.8,1)
                        .descelerate()
                        .duration(500)
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        Intent I = new Intent(Intent.ACTION_SEND);
                        I.setType("text/plain");
                        I.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
                        I.putExtra(Intent.EXTRA_SUBJECT,"Oferta - Desinquietos");
                        I.putExtra(Intent.EXTRA_TEXT,url_compartir);
                        startActivity(Intent.createChooser(I,"Compartir Oferta"));
                    }
                });

            }
        });
        btn_localizaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(btn_localizaciones)
                        .scale((float)0.8,1)
                        .descelerate()
                        .duration(500)
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        if(hayLocalizaciones) {
                            Intent I = new Intent(OfertaActivity.this, SedesActivity.class);
                            I.putExtra("oferta", oferta_id);
                            I.putExtra("establecimiento", text_title.getText().toString());
                            startActivity(I);
                        }else{
                            Toast.makeText(OfertaActivity.this, getResources().getString(R.string.err_localizaciones), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return false;
    }
}
