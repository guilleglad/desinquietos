package com.rider.desinquietos;

public class ItemObjects {
    private String subtitle;
    private String title;
    private String photo;
    private int photo_local;
    private int marco;
    private String codigo;
    private String isla;
    private String beneficio;
    private String Id_oferta;
    private String Descripcion;
    private String Id_empresa;
    private String domicilio;
    private String Municipio;
    private String cp;
    private String Provincia;
    private String Telefono_fijo;
    private String Telefono_movil;
    private String Email;
    private String Logo;
    private String Categoria;
    private String Subcategoria;
    private String url_compartir;
    private String url_web;

    public ItemObjects(String title, int marco, int photo_local, String cod, String subtitle, String isla, String beneficio){
        this.title = title;
        this.marco = marco;
        this.photo_local = photo_local;
        this.codigo = cod;
        this.subtitle = subtitle;
        this.isla = isla;
        this.beneficio = beneficio;
    }

    public ItemObjects(String title, int marco, String photo,String cod,String subtitle,String isla,String beneficio,String Id_oferta,String Descripcion
    , String Id_empresa,String domicilio,String Municipio,String cp,String Provincia,String Telefono_fijo
            ,String Telefono_movil,String Email,String Logo,String Categoria,String Subcategoria,String url_compartir,String url_web) {
        setTitle(title);
        setMarco(marco);
        setPhoto(photo);
        setCodigo(cod);
        setSubtitle(subtitle);
        setIsla(isla);
        setBeneficio(beneficio);
        setId_oferta(Id_oferta);
        setDescripcion(Descripcion);
        setId_empresa(Id_empresa);
        setDomicilio(domicilio);
        setMunicipio(Municipio);
        setCp(cp);
        setProvincia(Provincia);
        setTelefono_fijo(Telefono_fijo);
        setTelefono_movil(Telefono_movil);
        setEmail(Email);
        setLogo(Logo);
        setCategoria(Categoria);
        setSubcategoria(Subcategoria);
        setUrl_compartir(url_compartir);
        setUrl_web(url_web);
    }
    public ItemObjects(String title, int marco, int photo_local,String cod,String subtitle,String isla,String beneficio,String Id_oferta,String Descripcion
            , String Id_empresa,String domicilio,String Municipio,String cp,String Provincia,String Telefono_fijo
            ,String Telefono_movil,String Email,String Logo,String Categoria,String Subcategoria,String url_compartir,String url_web) {
        setTitle(title);
        setMarco(marco);
        setPhoto_local(photo_local);
        setCodigo(cod);
        setSubtitle(subtitle);
        setIsla(isla);
        setBeneficio(beneficio);
        setId_oferta(Id_oferta);
        setDescripcion(Descripcion);
        setId_empresa(Id_empresa);
        setDomicilio(domicilio);
        setMunicipio(Municipio);
        setCp(cp);
        setProvincia(Provincia);
        setTelefono_fijo(Telefono_fijo);
        setTelefono_movil(Telefono_movil);
        setEmail(Email);
        setLogo(Logo);
        setCategoria(Categoria);
        setSubcategoria(Subcategoria);
        setUrl_compartir(url_compartir);
        setUrl_web(url_web);
    }
    public String getUrl_compartir() {
        return url_compartir;
    }

    public void setUrl_compartir(String url_compartir) {
        this.url_compartir = url_compartir;
    }
    public String getId_oferta() {
        return Id_oferta;
    }

    public void setId_oferta(String id_oferta) {
        Id_oferta = id_oferta;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getId_empresa() {
        return Id_empresa;
    }

    public void setId_empresa(String id_empresa) {
        Id_empresa = id_empresa;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getMunicipio() {
        return Municipio;
    }

    public void setMunicipio(String municipio) {
        Municipio = municipio;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getProvincia() {
        return Provincia;
    }

    public void setProvincia(String provincia) {
        Provincia = provincia;
    }

    public String getTelefono_fijo() {
        return Telefono_fijo;
    }

    public void setTelefono_fijo(String telefono_fijo) {
        Telefono_fijo = telefono_fijo;
    }

    public String getTelefono_movil() {
        return Telefono_movil;
    }

    public void setTelefono_movil(String telefono_movil) {
        Telefono_movil = telefono_movil;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getLogo() {
        return Logo;
    }

    public void setLogo(String logo) {
        Logo = logo;
    }

    public String getCategoria() {
        return Categoria;
    }

    public void setCategoria(String categoria) {
        Categoria = categoria;
    }

    public String getSubcategoria() {
        return Subcategoria;
    }

    public void setSubcategoria(String subcategoria) {
        Subcategoria = subcategoria;
    }

    public String getBeneficio() {
        return beneficio;
    }

    public void setBeneficio(String beneficio) {
        this.beneficio = beneficio;
    }

    public String getIsla() {
        return isla;
    }

    public void setIsla(String isla) {
        this.isla = isla;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getMarco() {
        return marco;
    }

    public void setMarco(int marco) { this.marco = marco; }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getPhoto_local() {
        return photo_local;
    }

    public void setPhoto_local(int photo_local) {
        this.photo_local = photo_local;
    }

    public String getUrl_web() {
        return url_web;
    }

    public void setUrl_web(String url_web) {
        this.url_web = url_web;
    }
}
