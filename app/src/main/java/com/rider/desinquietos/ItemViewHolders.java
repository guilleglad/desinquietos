package com.rider.desinquietos;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;
import com.rider.desinquietos.misc.Constants;

/**
 * Created by Guillermo García on 08-08-2016.
 */
public class ItemViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView item_title;
    private ImageView item_marco;
    private SimpleDraweeView item_fondo;
    private String codigo;
    private TextView item_subtitle;
    private TextView item_isla;
    private TextView item_beneficio;
    private MainActivity myActivity;
    private FrameLayout Container;
    private String Id_oferta;
    private String Descripcion;
    private String Id_empresa;
    private String domicilio;
    private String Municipio;
    private String cp;
    private String Provincia;
    private String Telefono_fijo;
    private String Telefono_movil;
    private String Email;
    private String Logo;
    private String Categoria;
    private String Subcategoria;
    private String Foto;
    private String Compartir;
    private String Web;

    public ItemViewHolders(View itemView, MainActivity myActivity) {
        super(itemView);
        itemView.setOnClickListener(this);
        item_title = (TextView)itemView.findViewById(R.id.item_title);
        item_marco = (ImageView)itemView.findViewById(R.id.item_marco);
        item_fondo = (SimpleDraweeView) itemView.findViewById(R.id.item_fondo);
        item_subtitle = (TextView)itemView.findViewById(R.id.item_subtitle);
        item_isla = (TextView)itemView.findViewById(R.id.item_isla);
        item_beneficio = (TextView)itemView.findViewById(R.id.item_beneficio);
        item_beneficio.setSelected(true);
        this.myActivity = myActivity;
        this.Container = (FrameLayout)itemView.findViewById(R.id._layoutContainer);
    }

    public FrameLayout getContainer() {
        return Container;
    }

    public String getCompartir() {
        return Compartir;
    }

    public void setCompartir(String compartir) {
        Compartir = compartir;
    }
    public String getFoto() {
        return Foto;
    }

    public void setFoto(String foto) {
        Foto = foto;
    }
    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getId_empresa() {
        return Id_empresa;
    }

    public void setId_empresa(String id_empresa) {
        Id_empresa = id_empresa;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getMunicipio() {
        return Municipio;
    }

    public void setMunicipio(String municipio) {
        Municipio = municipio;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getProvincia() {
        return Provincia;
    }

    public void setProvincia(String provincia) {
        Provincia = provincia;
    }

    public String getTelefono_fijo() {
        return Telefono_fijo;
    }

    public void setTelefono_fijo(String telefono_fijo) {
        Telefono_fijo = telefono_fijo;
    }

    public String getTelefono_movil() {
        return Telefono_movil;
    }

    public void setTelefono_movil(String telefono_movil) {
        Telefono_movil = telefono_movil;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getLogo() {
        return Logo;
    }

    public void setLogo(String logo) {
        Logo = logo;
    }

    public String getCategoria() {
        return Categoria;
    }

    public void setCategoria(String categoria) {
        Categoria = categoria;
    }

    public String getSubcategoria() {
        return Subcategoria;
    }

    public void setSubcategoria(String subcategoria) {
        Subcategoria = subcategoria;
    }

    public String getId_oferta() {
        return Id_oferta;
    }

    public void setId_oferta(String id_oferta) {
        Id_oferta = id_oferta;
    }

    public void setContainer(FrameLayout container) {
        Container = container;
    }

    public TextView getItem_beneficio() {
        return item_beneficio;
    }

    public void setItem_beneficio(TextView item_beneficio) {
        this.item_beneficio = item_beneficio;
    }

    public TextView getItem_subtitle() {
        return item_subtitle;
    }

    public TextView getItem_isla() {
        return item_isla;
    }

    public void setItem_isla(TextView item_isla) {
        this.item_isla = item_isla;
    }

    public void setItem_subtitle(TextView item_subtitle) {
        this.item_subtitle = item_subtitle;
    }
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public TextView getItem_title() {
        return item_title;
    }

    public void setItem_title(TextView item_title) {
        this.item_title = item_title;
    }

    public ImageView getItem_marco() {
        return item_marco;
    }

    public void setItem_marco(ImageView item_marco) {
        this.item_marco = item_marco;
    }

    public ImageView getItem_fondo() {
        return item_fondo;
    }

    public void setItem_fondo(SimpleDraweeView item_fondo) {
        this.item_fondo = item_fondo;
    }


    public String getWeb() {
        return Web;
    }

    public void setWeb(String web) {
        Web = web;
    }

    @Override
    public void onClick(View view) {
        animateView(view);
        Intent I ;
        switch(this.codigo){
            case "juventud_canaria":
                String url = Constants.url_juventudcanaria;
                I = new Intent(Intent.ACTION_VIEW);
                I.setData(Uri.parse(url));
                this.myActivity.startActivity(I);
                break;
            case "como_solicitarlo":
                I =  new Intent(myActivity.getApplicationContext(),ComoObtenerlo.class);
                this.myActivity.startActivity(I);
                break;
            case "ventajas":
                I = new Intent(myActivity.getApplicationContext(),Cerca2Activity.class);
                this.myActivity.startActivity(I);
                break;
            default:
                I = new Intent(myActivity.getApplicationContext(),OfertaActivity.class);
                I.putExtra("Id_oferta",getId_oferta());
                I.putExtra("Nombre",getItem_subtitle().getText().toString());
                I.putExtra("Beneficio",getItem_beneficio().getText().toString());
                I.putExtra("Descripcion",getDescripcion());
                I.putExtra("Isla",getItem_isla().getText().toString());
                I.putExtra("Foto",getFoto());
                I.putExtra("Id_empresa",getId_empresa());
                I.putExtra("Establecimiento",getItem_title().getText().toString());
                I.putExtra("domicilio",getDomicilio());
                I.putExtra("Municipio",getMunicipio());
                I.putExtra("cp",getCp());
                I.putExtra("Provincia",getProvincia());
                I.putExtra("Telefono_fijo",getTelefono_fijo());
                I.putExtra("Telefono_movil",getTelefono_movil());
                I.putExtra("Email",getEmail());
                I.putExtra("Logo",getLogo());
                I.putExtra("Codigo",getCodigo());
                I.putExtra("Categoria",getCategoria());
                I.putExtra("Subcategoria",getSubcategoria());
                I.putExtra("Compartir",getCompartir());
                I.putExtra("Web",getWeb());
                this.myActivity.startActivity(I);
                break;
        }
    }

    private void animateView(View _view){
        ViewAnimator
                .animate(_view)
                .scale((float)0.8,1)
                .descelerate()
                .duration(500)
                .start();
    }


}
