package com.rider.desinquietos;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;
import com.rider.desinquietos.custom.AppController;
import com.rider.desinquietos.custom.MyEditText;
import com.rider.desinquietos.custom.MyTextView;
import com.rider.desinquietos.misc.Constants;

import org.json.JSONException;
import org.json.JSONObject;


public class LoginActivity extends AppCompatActivity implements MyEditText.OnEditorActionListener {

    private ImageView btn_navega;
    private ImageView btn_como_obtenerlo;
    private ImageButton btn_continue;
    private TextView texto_registrate;
    private LinearLayout input_dni;
    private StringRequest requestLogin;
    private MyEditText text_nif;
    private SharedPreferences myPrefs;
    private static String Nombre = "";
    private static String NIF = "";
    private static String Fecha_Nacimiento = "";
    private static String Sexo = "";
    private static String Email = "";
    private static String Telefono = "";
    private static String Direccion = "";
    private static String CP = "";
    private static String Fecha_Validez = "";
    private static String Foto = "";
    private static boolean logged_in=false;
    private ProgressBar pbar1;
    private MyEditText text_pass;
    private LinearLayout input_pass;

    @Override
    protected void onResume() {
        super.onResume();
        myPrefs = this.getSharedPreferences(Constants.myPrefs,this.MODE_PRIVATE);

        if(isLogged_in()){
            if(myPrefs.contains("Nombre")){
                setNombre(myPrefs.getString("Nombre",""));
            }
            if(myPrefs.contains("NIF")){
                setNIF(myPrefs.getString("NIF",""));
                if(getNIF().isEmpty()){
                    setLogged_in(false);
                }else{
                    setLogged_in(true);
                }
            }
            if(myPrefs.contains("Fecha_Nacimiento")){
                setFecha_Nacimiento(myPrefs.getString("Fecha_Nacimiento",""));
            }
            if(myPrefs.contains("Sexo")){
                setSexo(myPrefs.getString("Sexo",""));
            }
            if(myPrefs.contains("Email")){
                setEmail(myPrefs.getString("Email",""));
            }
            if(myPrefs.contains("Telefono")){
                setTelefono(myPrefs.getString("Telefono",""));
            }
            if(myPrefs.contains("Direccion")){
                setDireccion(myPrefs.getString("Direccion",""));
            }
            if(myPrefs.contains("CP")){
                setCP(myPrefs.getString("CP",""));
            }
            if(myPrefs.contains("Fecha_Validez")){
                setFecha_Validez(myPrefs.getString("Fecha_Validez",""));
            }
            if(myPrefs.contains("Foto")){
                setFoto(myPrefs.getString("Foto",""));
            }
            goto_Main();
        }
    }

    private void goto_Main() {
        pbar1.setVisibility(View.INVISIBLE);
        Intent I =  new Intent(getApplicationContext(),MainActivity.class);
        startActivity(I);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        texto_registrate = (TextView)findViewById(R.id.texto_registrate);
        input_dni = (LinearLayout)findViewById(R.id.input_dni);
        input_pass = (LinearLayout)findViewById(R.id.input_pass);
        btn_navega = (ImageView)findViewById(R.id.btn_navegar);
        btn_navega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                .animate(btn_navega)
                        .scale((float)0.8,1)
                        .descelerate()
                        .duration(500)
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        goto_Main();
                    }
                });
            }
        });
        btn_como_obtenerlo = (ImageView)findViewById(R.id.btn_como_obtenerlo);
        btn_como_obtenerlo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(btn_como_obtenerlo)
                        .scale((float)0.8,1)
                        .descelerate()
                        .duration(500)
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        Intent I =  new Intent(getApplicationContext(),ComoObtenerlo.class);
                        I.putExtra("back",false);
                        startActivity(I);
                        finish();
                    }
                });

            }
        });
        pbar1 = (ProgressBar)findViewById(R.id.progress_login);
        text_nif = (MyEditText)findViewById(R.id.text_nif);
        text_nif.setOnEditorActionListener(this);
        text_pass = (MyEditText)findViewById(R.id.text_pass);
        text_pass.setOnEditorActionListener(this);
        btn_continue = (ImageButton)findViewById(R.id.btn_continue);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        btn_continue.setImageDrawable(upArrow);
        btn_continue.setRotationY(180);
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(btn_continue)
                        .scale((float)0.8,1)
                        .descelerate()
                        .duration(500)
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        pbar1.setVisibility(View.VISIBLE);
                        requestLogin =new StringRequest(Request.Method.GET, Constants.url + "/" + Constants.url_login
                                + "?nif=" + text_nif.getText().toString() + "&clave=" + text_pass.getText()
                         , new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                if(!response.isEmpty()){
                                    try {
                                        JSONObject jres = new JSONObject(response);
                                        if(jres.has("code")){
                                            if(jres.getString("code").equals("400")){
                                                Toast.makeText(getApplicationContext(),jres.getString("message"),Toast.LENGTH_LONG).show();
                                                pbar1.setVisibility(View.INVISIBLE);
                                            }
                                        }else{
                                            SharedPreferences.Editor _editor = myPrefs.edit();
                                            _editor.putString("Nombre",jres.getString("Nombre"));
                                            _editor.putString("NIF",jres.getString("NIF"));
                                            _editor.putString("Fecha_Nacimiento",jres.getString("Fecha_Nacimiento"));
                                            _editor.putString("Sexo",jres.getString("Sexo"));
                                            _editor.putString("Email",jres.getString("Email"));
                                            _editor.putString("Telefono",jres.getString("Telefono"));
                                            _editor.putString("Direccion",jres.getString("Direccion"));
                                            _editor.putString("CP",jres.getString("CP"));
                                            _editor.putString("Fecha_Validez",jres.getString("Fecha_Validez"));
                                            _editor.putString("Foto",jres.getString("Foto"));
                                            if(_editor.commit()) {
                                                setNombre(jres.getString("Nombre"));
                                                setNIF(jres.getString("NIF"));
                                                setFecha_Nacimiento(jres.getString("Fecha_Nacimiento"));
                                                setSexo(jres.getString("Sexo"));
                                                setEmail(jres.getString("Email"));
                                                setTelefono(jres.getString("Telefono"));
                                                setDireccion(jres.getString("Direccion"));
                                                setCP(jres.getString("CP"));
                                                setFecha_Validez(jres.getString("Fecha_Validez"));
                                                setFoto(jres.getString("Foto"));
                                                setLogged_in(true);
                                                goto_Main();
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pbar1.setVisibility(View.INVISIBLE);
                                    }

                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(),getResources().getString(R.string.err_nif),Toast.LENGTH_LONG).show();
                                pbar1.setVisibility(View.INVISIBLE);
                            }
                        });
                        addRequest(requestLogin);
                }});
            }

        });

        ViewAnimator
                .animate(texto_registrate,input_dni,input_pass)
                .alpha(0,1)
                .slideTop()
                .duration(1000)
                .thenAnimate(btn_navega)
                .alpha(0,1)
                .slideLeft()
                .duration(500)
                .thenAnimate(btn_como_obtenerlo)
                .alpha(0,1)
                .slideRight()
                .duration(500)
                .start();
    }

    public void addRequest(StringRequest requestLogin){
        requestLogin.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(requestLogin,"Profile");
    }

    public static boolean isLogged_in() {
        SharedPreferences myPrefs = AppController.getInstance().getSharedPreferences(Constants.myPrefs,MODE_PRIVATE);
        boolean res= myPrefs.getBoolean("logged_in",false);
        if(res) {
            LoginActivity.logged_in = true;
            return true;
        }
        else {
            LoginActivity.logged_in = false;
            return false;
        }

    }

    public static void setLogged_in(boolean logged_in) {
        SharedPreferences myPrefs = AppController.getInstance().getSharedPreferences(Constants.myPrefs,MODE_PRIVATE);
        SharedPreferences.Editor _editor = myPrefs.edit();
        _editor.putBoolean("logged_in",logged_in);
        if(_editor.commit())
            LoginActivity.logged_in = logged_in;

    }

    public static String getFoto() {
        return Foto;
    }

    public static void setFoto(String foto) {
        Foto = foto;
    }

    public static String getNombre() {
        return Nombre;
    }

    public static void setNombre(String nombre) {
        Nombre = nombre;
    }

    public static String getNIF() {
        return NIF;
    }

    public static void setNIF(String NIF) {
        LoginActivity.NIF = NIF;
    }

    public static String getFecha_Nacimiento() {
        return Fecha_Nacimiento;
    }

    public static void setFecha_Nacimiento(String fecha_Nacimiento) {
        Fecha_Nacimiento = fecha_Nacimiento;
    }

    public static String getSexo() {
        return Sexo;
    }

    public static void setSexo(String sexo) {
        Sexo = sexo;
    }

    public static String getEmail() {
        return Email;
    }

    public static void setEmail(String email) {
        Email = email;
    }

    public static String getTelefono() {
        return Telefono;
    }

    public static void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public static String getDireccion() {
        return Direccion;
    }

    public static void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public static String getCP() {
        return CP;
    }

    public static void setCP(String CP) {
        LoginActivity.CP = CP;
    }

    public static String getFecha_Validez() {
        return Fecha_Validez;
    }

    public static void setFecha_Validez(String fecha_Validez) {
        Fecha_Validez = fecha_Validez;
    }


    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if(actionId== EditorInfo.IME_ACTION_SEND){
            btn_continue.callOnClick();
            return true;
        }
        return false;
    }
}
