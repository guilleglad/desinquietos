package com.rider.desinquietos;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SubMenu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.drawee.view.SimpleDraweeView;
import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;
import com.rider.desinquietos.custom.AppController;
import com.rider.desinquietos.custom.CustomTypefaceSpan;
import com.rider.desinquietos.custom.MyEditText;
import com.rider.desinquietos.misc.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.voghdev.pdfviewpager.library.asset.CopyAsset;
import es.voghdev.pdfviewpager.library.asset.CopyAssetThreadImpl;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MyEditText.OnEditorActionListener{

    private StaggeredGridLayoutManager MyGridLayoutManager;
    public SwipeRefreshLayout mSwipeRefreshLayout;
    private List<ItemObjects> ListadoDinamico;
    private ItemRecyclerViewAdapter rcAdapter;
    private  Map<String,Integer> marcos;
    private Map<String, Integer> marcos_destacados;
    private static boolean novedades = false;
    private RecyclerView recycler_view;
    private ImageView img_actionbar;
    private Bundle extras;
    private boolean back_active = false;
    private Parcelable layoutManagerSavedState;
    private boolean recargar = true;
    private long back_pressed;
    private SimpleDraweeView btn_profile;
    private SharedPreferences myPrefs;
    private MyEditText txt_buscar;
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 111;
    private String url_facebook="",url_twitter="",url_instagram="",url_snapchat="",url_youtube="";
    private ImageButton btn_facebook;
    private ImageButton btn_twitter;
    private ImageButton btn_instagram;
    private ImageButton btn_snapchat;
    private ImageButton btn_youtube;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        carga_links_redessociales();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        createpdfcache();

        extras = getIntent().getExtras();
        if(extras != null){
            back_active = extras.getBoolean("back_active");
        }
        getSupportActionBar().setDisplayShowHomeEnabled(back_active);
        getSupportActionBar().setDisplayHomeAsUpEnabled(back_active);
        getSupportActionBar().setHomeButtonEnabled(back_active);
        getSupportActionBar().setTitle("");
        img_actionbar = (ImageView)findViewById(R.id.img_actionbar);
        img_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isTaskRoot()) {
                    Intent I = new Intent(getApplicationContext(), MainActivity.class);
                    I.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(I);
                    finish();
                }
            }
        });
        marcos = new HashMap<String,Integer>();
        marcos.put("CL",R.mipmap.marco_cl);
        marcos.put("TC",R.mipmap.marco_tc);
        marcos.put("HB",R.mipmap.marco_hb);
        marcos.put("SP",R.mipmap.marco_sp);
        marcos.put("FD",R.mipmap.marco_fd);
        marcos.put("AC",R.mipmap.marco_ac);
        marcos.put("SV",R.mipmap.marco_sv);
        marcos.put("SH",R.mipmap.marco_sh);
        marcos.put("TT",R.mipmap.marco_tt);
        marcos_destacados = new HashMap<String,Integer>();
        marcos_destacados.put("CL",R.mipmap.marco_cl_d);
        marcos_destacados.put("TC",R.mipmap.marco_tc_d);
        marcos_destacados.put("HB",R.mipmap.marco_hb_d);
        marcos_destacados.put("SP",R.mipmap.marco_sp_d);
        marcos_destacados.put("FD",R.mipmap.marco_fd_d);
        marcos_destacados.put("AC",R.mipmap.marco_ac_d);
        marcos_destacados.put("SV",R.mipmap.marco_sv_d);
        marcos_destacados.put("SH",R.mipmap.marco_sh_d);
        marcos_destacados.put("TT",R.mipmap.marco_tt_d);
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action_received", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        if(!back_active) {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            navigationView.setItemIconTintList(null);

            txt_buscar = (MyEditText)navigationView.getHeaderView(0).findViewById(R.id.txt_buscar);
            txt_buscar.setOnEditorActionListener(this);


            Menu m = navigationView.getMenu();
            for (int i = 0; i < m.size(); i++) {
                MenuItem mi = m.getItem(i);

                //for aapplying a font to subMenu ...
                SubMenu subMenu = mi.getSubMenu();
                if (subMenu != null && subMenu.size() > 0) {
                    for (int j = 0; j < subMenu.size(); j++) {
                        MenuItem subMenuItem = subMenu.getItem(j);
                        applyFontToMenuItem(subMenuItem, false);
                    }
                }

                //the method we have create in activity
                applyFontToMenuItem(mi, true);

                try {

                    btn_facebook = (ImageButton) mi.getActionView().findViewById(R.id.img_facebook);
                    if (btn_facebook != null) {
                        btn_facebook.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ViewAnimator
                                        .animate(btn_facebook)
                                        .scale((float)0.8,1)
                                        .duration(500)
                                        .descelerate()
                                        .start().onStop(new AnimationListener.Stop() {
                                    @Override
                                    public void onStop() {
                                        if(!url_facebook.isEmpty()){
                                            Intent I = new Intent(Intent.ACTION_VIEW);
                                            I.setData(Uri.parse(url_facebook));
                                            startActivity(I);
                                        }
                                    }
                                });

                            }
                        });
                    }
                    btn_twitter = (ImageButton) mi.getActionView().findViewById(R.id.img_twitter);
                    if (btn_twitter != null) {
                        btn_twitter.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                {
                                    ViewAnimator
                                            .animate(btn_twitter)
                                            .scale((float)0.8,1)
                                            .duration(500)
                                            .descelerate()
                                            .start().onStop(new AnimationListener.Stop() {
                                        @Override
                                        public void onStop() {
                                            if(!url_twitter.isEmpty()){
                                                Intent I = new Intent(Intent.ACTION_VIEW);
                                                I.setData(Uri.parse(url_twitter));
                                                startActivity(I);
                                            }
                                        }
                                    });

                                }
                            }
                        });
                    }
                    btn_instagram = (ImageButton) mi.getActionView().findViewById(R.id.img_instagram);
                    if (btn_instagram != null) {
                        btn_instagram.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                {
                                    ViewAnimator
                                            .animate(btn_instagram)
                                            .scale((float)0.8,1)
                                            .duration(500)
                                            .descelerate()
                                            .start().onStop(new AnimationListener.Stop() {
                                        @Override
                                        public void onStop() {
                                            if(!url_instagram.isEmpty()){
                                                Intent I = new Intent(Intent.ACTION_VIEW);
                                                I.setData(Uri.parse(url_instagram));
                                                startActivity(I);
                                            }
                                        }
                                    });

                                }
                            }
                        });
                    }
                    btn_snapchat = (ImageButton) mi.getActionView().findViewById(R.id.img_snapchat);
                    if (btn_snapchat != null) {
                        btn_snapchat.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                {
                                    ViewAnimator
                                            .animate(btn_snapchat)
                                            .scale((float)0.8,1)
                                            .duration(500)
                                            .descelerate()
                                            .start().onStop(new AnimationListener.Stop() {
                                        @Override
                                        public void onStop() {
                                            if(!url_snapchat.isEmpty()){
                                                Intent I = new Intent(Intent.ACTION_VIEW);
                                                I.setData(Uri.parse(url_snapchat));
                                                startActivity(I);
                                            }
                                        }
                                    });

                                }
                            }
                        });
                    }
                    btn_youtube = (ImageButton) mi.getActionView().findViewById(R.id.img_youtube);
                    if (btn_youtube != null) {
                        btn_youtube.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                {
                                    ViewAnimator
                                            .animate(btn_youtube)
                                            .scale((float)0.8,1)
                                            .duration(500)
                                            .descelerate()
                                            .start().onStop(new AnimationListener.Stop() {
                                        @Override
                                        public void onStop() {
                                            if(!url_youtube.isEmpty()){
                                                Intent I = new Intent(Intent.ACTION_VIEW);
                                                I.setData(Uri.parse(url_youtube));
                                                startActivity(I);
                                            }
                                        }
                                    });

                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        recycler_view.setHasFixedSize(true);


        MyGridLayoutManager = new StaggeredGridLayoutManager(2,1);
        recycler_view.setLayoutManager(MyGridLayoutManager);
        ListadoDinamico = new ArrayList<ItemObjects>();
        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showInitialItems();
            }
        });
        showInitialItems();
        rcAdapter = new ItemRecyclerViewAdapter(getApplicationContext(), ListadoDinamico,this);
        recycler_view.setAdapter(rcAdapter);


        btn_profile = (SimpleDraweeView)findViewById(R.id.btn_profile);
        btn_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(btn_profile)
                        .scale((float)0.8,1)
                        .duration(500)
                        .descelerate()
                        .start().onStop(new AnimationListener.Stop() {
                            @Override
                            public void onStop() {
                                if (LoginActivity.isLogged_in()) {
                                    Intent I = new Intent(getApplicationContext(), FichaActivity.class);
                                    startActivity(I);
                                } else {
                                    Intent I = new Intent(getApplicationContext(), LoginActivity.class);
                                    startActivity(I);
                                    finish();
                                }
                            }
                        });
            }
        });


        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.ACCESS_COARSE_LOCATION};

        if(!checkPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

    }

    private void carga_links_redessociales() {
        StringRequest requestRedes = new StringRequest(Request.Method.GET, Constants.url + "/" + Constants.url_redes, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(!response.isEmpty()){
                    try {
                        JSONObject jres = new JSONObject(response);
                        url_facebook = jres.getString("Facebook");
                        url_instagram = jres.getString("Instagram");
                        url_snapchat = jres.getString("Snapchat");
                        url_youtube = jres.getString("Youtube");
                        url_twitter = jres.getString("Twitter");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        requestRedes.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(requestRedes,"Redes");
    }

    private void createpdfcache() {
        File cacheDir = new File(getApplicationContext().getCacheDir(), "cache");
        File cacheFile = new File(cacheDir,"bases.pdf");
        if(!cacheFile.exists()) {
            CopyAsset copyAsset = new CopyAssetThreadImpl(getApplicationContext(), new Handler());
            try {
                copyAsset.copy("bases_legales.pdf", new File(getCacheDir(), "bases.pdf").getAbsolutePath());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void showInitialItems(){
        if(extras == null){
            mSwipeRefreshLayout.setRefreshing(true);
            MainActivity.novedades = false;
            getListItemData(Constants.url_destacadas,"",true,false,1);
        }else{
            mSwipeRefreshLayout.setRefreshing(true);
            MainActivity.novedades = extras.getBoolean("novedades");
            getListItemData(extras.getString("url"), extras.getString("cat"), extras.getBoolean("header"), extras.getBoolean("add"), extras.getInt("count"));
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(btn_profile!=null && LoginActivity.isLogged_in())
            btn_profile.setImageURI(Uri.parse(LoginActivity.getFoto()));
    }

    public void getListItemData(final String action, String p, final boolean header, final boolean add, final int count){
        final List<ItemObjects> listViewItems = new ArrayList<ItemObjects>();
        Map<String, String> params = new HashMap<String, String>();

        StringRequest ListadoCategoriasReq = new StringRequest(Request.Method.GET, Constants.url + "/"+action+"?"+p, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                int c = 1;
                Log.i("RESPONSE",response);
                if(!response.isEmpty()){
                    try {
                        JSONObject j_obj = new JSONObject(response);
                        if(j_obj != null){
                            if(header) {
                                listViewItems.add(new ItemObjects("", 0, R.mipmap.img_ventajas, "ventajas","","",""));
                                listViewItems.add(new ItemObjects("", 0, R.mipmap.logo_juventud_home, "juventud_canaria","","",""));
                                listViewItems.add(new ItemObjects("", 0, R.mipmap.img_comosolicitarlo, "como_solicitarlo","","",""));
                            }
                            if(!add) {
                                rcAdapter.clear();
                            }
                            JSONArray j_cat = j_obj.getJSONArray("Ofertas");
                            if(j_cat.length() == 0) {
                                Toast.makeText(getBaseContext(), getResources().getString(R.string.msg_no_result),
                                        Toast.LENGTH_LONG).show();
                                return;
                            }
                            //JSONArray j_cat = j_obj.getJSONArray(Character.toString(action_received.charAt(0)).toUpperCase()+action_received.substring(1));
                            if(count == 0) {
                                c = j_cat.length();
                            }else{
                                c = count;
                            }

                            for(int i = 0;i<c;i++){
                                JSONObject item = j_cat.getJSONObject(i);
                                Object marco;
                                if(action.equals(Constants.url_destacadas)) {
                                    marco = marcos_destacados.get(item.getString("Codigo"));
                                    if(marco==null){
                                        marco=marcos_destacados.get("FD");
                                    }
                                    listViewItems.add(new ItemObjects(item.getString("Establecimiento"), (int)marco, item.getString("Foto")
                                            , item.getString("Codigo"), item.getString("Nombre"), item.getString("Isla"), item.getString("Beneficio"),item.getString("Id_oferta"),item.getString("Descripcion")
                                            , item.getString("Id_empresa"),item.getString("domicilio"),item.getString("Municipio"),item.getString("cp"),item.getString("Provincia"),item.getString("Telefono_fijo")
                                            ,item.getString("Telefono_movil"),item.getString("Email"),item.getString("Logo"),item.getString("Categoria"),item.getString("Subcategoria"),item.getString("Compartir"),item.getString("Web")));
                                }else {
                                    marco = marcos.get(item.getString("Codigo"));
                                    if(marco==null){
                                        marco=marcos.get("FD");
                                    }
                                    listViewItems.add(new ItemObjects(item.getString("Establecimiento"),(int)marco , item.getString("Foto")
                                            , item.getString("Codigo"), item.getString("Nombre"), item.getString("Isla"), item.getString("Beneficio"),item.getString("Id_oferta"),item.getString("Descripcion")
                                            , item.getString("Id_empresa"),item.getString("domicilio"),item.getString("Municipio"),item.getString("cp"),item.getString("Provincia"),item.getString("Telefono_fijo")
                                            ,item.getString("Telefono_movil"),item.getString("Email"),item.getString("Logo"),item.getString("Categoria"),item.getString("Subcategoria"),item.getString("Compartir"),item.getString("Web")));
                                }
                            }
                            if(!MainActivity.novedades){
                                getListItemData(Constants.url_novedades,"",false,true,0);
                                MainActivity.novedades = true;
                            }
                            rcAdapter.addAll(listViewItems);
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        ListadoCategoriasReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(ListadoCategoriasReq,"Listado");
    }
    private void applyFontToMenuItem(MenuItem mi,boolean bold) {
        Typeface font;
        if(!bold)
            font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        else
            font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Bold.ttf");

        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mNewTitle.setSpan(new ForegroundColorSpan(Color.WHITE),0,mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if (!this.isTaskRoot()) {
                super.onBackPressed();
            } else {
                if (back_pressed + Constants.TIME_DELAY > System.currentTimeMillis()) {
                    super.onBackPressed();
                } else {
                    Toast.makeText(getBaseContext(), getResources().getString(R.string.msg_antes_salir),
                            Toast.LENGTH_SHORT).show();
                }
                back_pressed = System.currentTimeMillis();
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action_received bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action_received bar item clicks here. The action_received bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        //CATEGORIAS
        if (id == R.id.sidebar_cultura_ocio){
//            MainActivity.novedades = true;
//            getListItemData(Constants.url_ofertas,"categoria=CL",false,false,0);
            createIntent(true,Constants.url_ofertas,"categoria=CL",false,false,0);
        }
        if (id == R.id.sidebar_formacion_cursos){
//            MainActivity.novedades = true;
//            getListItemData(Constants.url_ofertas,"categoria=TC",false,false,0);
            createIntent(true,Constants.url_ofertas,"categoria=TC",false,false,0);
        }
        if (id == R.id.sidebar_salud_belleza){
//            MainActivity.novedades = true;
//            getListItemData(Constants.url_ofertas,"categoria=HB",false,false,0);
            createIntent(true,Constants.url_ofertas,"categoria=HB",false,false,0);
        }
        if (id == R.id.sidebar_deportes){
//            MainActivity.novedades = true;
//            getListItemData(Constants.url_ofertas,"categoria=SP",false,false,0);
            createIntent(true,Constants.url_ofertas,"categoria=SP",false,false,0);
        }
        if (id == R.id.sidebar_comida_bebida){
//            MainActivity.novedades = true;
//            getListItemData(Constants.url_ofertas,"categoria=FD",false,false,0);
            createIntent(true,Constants.url_ofertas,"categoria=FD",false,false,0);
        }
        if (id == R.id.sidebar_alojamientos){
//            MainActivity.novedades = true;
//            getListItemData(Constants.url_ofertas,"categoria=AC",false,false,0);
            createIntent(true,Constants.url_ofertas,"categoria=AC",false,false,0);
        }
        if (id == R.id.sidebar_servicios){
//            MainActivity.novedades = true;
//            getListItemData(Constants.url_ofertas,"categoria=SV",false,false,0);
            createIntent(true,Constants.url_ofertas,"categoria=SV",false,false,0);
        }
        if (id == R.id.sidebar_compras){
//            MainActivity.novedades = true;
//            getListItemData(Constants.url_ofertas,"categoria=SH",false,false,0);
            createIntent(true,Constants.url_ofertas,"categoria=SH",false,false,0);
        }
        if (id == R.id.sidebar_viajes_transporte){
//            MainActivity.novedades = true;
//            getListItemData(Constants.url_ofertas,"categoria=TT",false,false,0);
            createIntent(true,Constants.url_ofertas,"categoria=TT",false,false,0);
        }
        //DESTACADOS
        if (id == R.id.sidebar_destacados){
//            MainActivity.novedades = true;
//            getListItemData(Constants.url_destacadas,"",false,false,0);
            createIntent(true,Constants.url_destacadas,"",false,false,0);
        }
        if(id==R.id.sidebar_solicitalo){
            Intent I = new Intent(getApplicationContext(),ComoObtenerlo.class);
            I.putExtra("back",true);
            startActivity(I);
        }
        if(id==R.id.sidebar_juventud_canaria){
            String url = Constants.url_juventudcanaria;
            Intent I = new Intent(Intent.ACTION_VIEW);
            I.setData(Uri.parse(url));
            startActivity(I);
        }
        if(id==R.id.sidebar_contacto){
            Intent I = new Intent(getApplicationContext(),ContactoActivity.class);
            startActivity(I);
        }
        if(id==R.id.sidebar_cerrarsesion){
            myPrefs = this.getSharedPreferences(Constants.myPrefs,MODE_PRIVATE);
            SharedPreferences.Editor _editor = myPrefs.edit();
            _editor.clear();
            _editor.commit();
            LoginActivity.setLogged_in(false);
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.sesion_cerrada),Toast.LENGTH_LONG).show();
            Intent I = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(I);
            finish();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void createIntent(boolean novedades, String url,String p,boolean h,boolean a,int c){
        Intent I = new Intent(getApplicationContext(),MainActivity.class);
        I.putExtra("novedades",novedades);
        I.putExtra("url",url);
        I.putExtra("cat",p);
        I.putExtra("header",h);
        I.putExtra("add",a);
        I.putExtra("count",c);
        startActivity(I);
    }


    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if(actionId == EditorInfo.IME_ACTION_SEARCH){
            createIntent(true,Constants.url_buscar,"q="+v.getText().toString(),false,false,0);
            return true;
        }
        return false;
    }

    private static boolean checkPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


}
