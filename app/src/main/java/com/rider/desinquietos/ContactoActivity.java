package com.rider.desinquietos;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;
import com.rider.desinquietos.misc.Constants;

import java.util.Locale;

public class ContactoActivity extends AppCompatActivity {

    private SimpleDraweeView btn_profile;
    private Button btn_tenerife,btn_laguna,btn_grancanaria;
    private RelativeLayout layout_ven;
    private ImageView btn_correo,btn_llamar,btn_fb,btn_twitter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacto);
//TOOLBAR
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.indicator), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        //TOOLBAR*
        btn_correo = (ImageView)findViewById(R.id.btn_correo);
        btn_correo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(btn_correo)
                        .scale((float)0.8,1)
                        .descelerate()
                        .duration(500)
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("message/rfc822");
                        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{Constants.email});
                        i.putExtra(Intent.EXTRA_SUBJECT, "Contacto Desinquietos");
                        i.putExtra(Intent.EXTRA_TEXT   , "");
                        try {
                            startActivity(Intent.createChooser(i, "Enviar Correo..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(ContactoActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
        btn_llamar = (ImageView)findViewById(R.id.btn_llamar);
        btn_llamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(btn_llamar)
                        .scale((float)0.8,1)
                        .descelerate()
                        .duration(500)
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        Intent i = new Intent(Intent.ACTION_DIAL);
                        String uri = String.format("tel:"+Constants.telefono.trim());
                        i.setData(Uri.parse(uri));
                        startActivity(i);
                    }
                });
            }
        });
        btn_fb = (ImageView)findViewById(R.id.btn_fb);
        btn_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(btn_fb)
                        .scale((float)0.8,1)
                        .descelerate()
                        .duration(500)
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        String uri = String.format(Constants.url_fb);
                        i.setData(Uri.parse(uri));
                        startActivity(i);
                    }
                });
            }
        });
        btn_twitter = (ImageView)findViewById(R.id.btn_twitter);
        btn_twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(btn_twitter)
                        .scale((float)0.8,1)
                        .descelerate()
                        .duration(500)
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        String uri = String.format(Constants.url_twitter);
                        i.setData(Uri.parse(uri));
                        startActivity(i);
                    }
                });
            }
        });
        btn_profile = (SimpleDraweeView)findViewById(R.id.btn_profile);
        btn_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(btn_profile)
                        .scale((float)0.8,1)
                        .duration(500)
                        .descelerate()
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        if (LoginActivity.isLogged_in()) {
                            Intent I = new Intent(getApplicationContext(), FichaActivity.class);
                            startActivity(I);
                        } else {
                            Intent I = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(I);
                            finish();
                        }
                    }
                });
            }
        });
        btn_profile.setImageURI(Uri.parse(LoginActivity.getFoto()));
        layout_ven = (RelativeLayout)findViewById(R.id.layout_ven);
        btn_laguna = (Button)findViewById(R.id.btn_laguna);
        btn_laguna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(layout_ven)
                        .bounce()
                        .duration(500)
                        .descelerate()
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        String label = Constants.laguna_txt;
                        String uri = String.format(Locale.ENGLISH, "geo:%f,%f?q=%f,%f(%s)", Constants.laguna_lat, Constants.laguna_lon, Constants.laguna_lat, Constants.laguna_lon,label);
                        Intent I = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        startActivity(I);
                    }
                });

            }
        });
        btn_tenerife = (Button)findViewById(R.id.btn_tenerife);
        btn_tenerife.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(layout_ven)
                        .bounce()
                        .duration(500)
                        .descelerate()
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                String label = Constants.laguna_txt;
                String uri = String.format(Locale.ENGLISH, "geo:%f,%f?q=%f,%f(%s)", Constants.tenerife_lat, Constants.tenerife_lon, Constants.tenerife_lat, Constants.tenerife_lon,label);
                Intent I = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(I);
                    }
                });
            }
        });
        btn_grancanaria = (Button)findViewById(R.id.btn_grancanaria);
        btn_grancanaria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(layout_ven)
                        .bounce()
                        .duration(500)
                        .descelerate()
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        String label = Constants.grancanaria_txt;
                String uri = String.format(Locale.ENGLISH, "geo:%f,%f?q=%f,%f(%s)", Constants.grancanaria_lat, Constants.grancanaria_lon, Constants.grancanaria_lat, Constants.grancanaria_lon,label);
                Intent I = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(I);
                }
            });
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }
}
