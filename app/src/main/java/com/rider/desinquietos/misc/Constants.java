package com.rider.desinquietos.misc;

import java.util.Locale;

/**
 * Created by Guillermo García on 31-07-2016.
 */

public class Constants {
    public static final int SPLASH_SCREEN_DELAY = 2000;
    public static final String url = "http://94.23.42.106/ws";
    public static final String url_categorias = "categorias";
    public static final String url_subcategorias = "subcategorias";
    public static final String url_destacadas = "destacadas";
    public static final String url_novedades = "novedades";
    public static final String url_ofertas= "ofertas";
    public static final String url_logoempresa = "LogoEmpresa.aspx";
    public static final String url_fotooferta = "FotoOferta.aspx";
    public static final String json_header = "ofertas";
    public static final String url_buscar = "buscarOfertas";
    public static final String url_subirfoto = "AddFotoUsuario";
    public static final String url_sedes = "sedes";
    public static final String url_cerca = "ofertasCercanas";
    public static final float MAP_ZOOM = 15;
    public static final long TIME_DELAY = 2000;
    public static int map_padding_localizaciones = 200;
    public static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    public static String url_juventudcanaria = "http://www.juventudcanaria.com";
    public static String url_solicitame="http://www.desinquietos.es/solicitame";
    public static String url_usuario="usuario";
    public static String url_login="login";
    public static String myPrefs = "MyPrefs";
    public static String url_laguna = "https://goo.gl/maps/yPuLx4XLyn12";
    public static float laguna_lat = 28.492594f;
    public static float laguna_lon = -16.324448f;
    public static String url_tenerife = "https://goo.gl/maps/tuLFqaUxwwQ2";
    public static float tenerife_lat = 28.461003f;
    public static float tenerife_lon = -16.250505f;
    public static String url_grancanaria = "https://goo.gl/maps/xRW1WPzMXUv";
    public static float grancanaria_lat = 28.111412f;
    public static float grancanaria_lon = -15.417267f;
    public static String laguna_txt = "Centro Atlántico de la Juventud Wenceslao Yanes González, 1738202 La Laguna";
    public static String tenerife_txt = "Avda. José Manuel Guimera, nº 10 Edf. Servicios Múltiples II, planta 2ª 38071 Santa Cruz de Tenerife";
    public static String grancanaria_txt = "Profesor Agustín Millares Carló, 18 Edificio de Servicios Múltiples II - 4ª planta 35003 Las Palmas de Gran Canaria";
    public static String email = "contacto@desinquietos.com";
    public static String telefono = "928 306397";
    public static String url_fb="http://www.facebook.com";
    public static String url_twitter = "http://www.twitter.com";
    public static String url_redes = "socials";


    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 60000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;



}
