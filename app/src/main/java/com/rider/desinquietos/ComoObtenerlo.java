package com.rider.desinquietos;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;
import com.rider.desinquietos.misc.Constants;

public class ComoObtenerlo extends AppCompatActivity {

    private ImageView btn_requisitos;
    private ImageView img_requisitos;
    private ImageView img_req_text1;
    private ImageView tab_req;
    private ImageView img_req_text2;
    private ImageView tab_donde;
    private ImageView img_donde;
    private ImageView img_donde_texto;
    private ImageView btn_donde;
    private Bundle extras;
    private boolean back;
    private SimpleDraweeView btn_profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if((extras = getIntent().getExtras()) != null)
        {
            back = extras.getBoolean("back");
        }else{
            back = true;
        }
        setContentView(R.layout.activity_como_obtenerlo);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.indicator), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

//        tab_req =(ImageView)findViewById(R.id.tab_req);
        img_requisitos =(ImageView)findViewById(R.id.img_fondo_req);
        img_req_text1 = (ImageView)findViewById(R.id.img_text_req1);
        img_req_text2 = (ImageView)findViewById(R.id.img_text_req2);
        img_req_text2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(v)
                        .scale((float)0.8,1)
                        .descelerate()
                        .duration(500)
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        Intent I = new Intent(Intent.ACTION_VIEW);
                        I.setData(Uri.parse(Constants.url_solicitame));
                        startActivity(I);
                    }
                });



            }
        });

//        tab_donde = (ImageView)findViewById(R.id.tab_donde);
        img_donde=(ImageView)findViewById(R.id.img_fondo_donde);
        img_donde_texto= (ImageView)findViewById(R.id.img_text_donde);

        btn_requisitos = (ImageView)findViewById(R.id.btn_requisitos);
        btn_requisitos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(img_requisitos.getAlpha()==0){
                    ViewAnimator
                            .animate(img_requisitos,img_req_text1,img_req_text2)
                            .alpha(0,1)
                            .duration(500)
                            .thenAnimate(btn_donde)
                            .scale(1,(float)0.9)
                            .duration(250)
                            .start();
                }
                if(btn_requisitos.getScaleX()==(float)0.9){
                    ViewAnimator
                            .animate(btn_requisitos)
                            .scale((float)0.9,1)
                            .duration(250)
                            .start();
                }
                if(img_donde.getAlpha()==1){
                    ViewAnimator
                    .animate(img_donde,img_donde_texto)
                            .alpha(1,0)
                            .duration(250)
                            .start();
                }
            }
        });
        btn_donde = (ImageView)findViewById(R.id.btn_dondesolicitarlo);
        btn_donde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(img_donde.getAlpha()==0){
                    ViewAnimator
                            .animate(img_donde,img_donde_texto)
                            .alpha(0,1)
                            .duration(500)
                            .thenAnimate(btn_requisitos)
                            .scale(1,(float)0.9)
                            .duration(250)
                            .start();
                }
                if(btn_donde.getScaleX()==(float)0.9){
                    ViewAnimator
                            .animate(btn_donde)
                            .scale((float)0.9,1)
                            .duration(250)
                            .start();
                }
                if(img_requisitos.getAlpha()==1){
                    ViewAnimator
                            .animate(img_requisitos,img_req_text1,img_req_text2)
                            .alpha(1,0)
                            .duration(250)
                            .start();
                }
            }
        });
        btn_profile = (SimpleDraweeView)findViewById(R.id.btn_profile);
        btn_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(btn_profile)
                        .scale((float)0.8,1)
                        .duration(500)
                        .descelerate()
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        if (LoginActivity.isLogged_in()) {
                            Intent I = new Intent(getApplicationContext(), FichaActivity.class);
                            startActivity(I);
                        } else {
                            Intent I = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(I);
                            finish();
                        }
                    }
                });
            }
        });
        btn_profile.setImageURI(Uri.parse(LoginActivity.getFoto()));
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }
    public void startMainActivity(){
        Intent I =  new Intent(getApplicationContext(),MainActivity.class);
        I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(I);
        finish();
    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        if(back) {
            //return super.onSupportNavigateUp();
            super.onBackPressed();
            return false;
        }
        else {
            startMainActivity();
            return false;
        }

    }

    @Override
    public void onBackPressed() {
        finish();
        if(back)
//            NavUtils.navigateUpFromSameTask(this);
            super.onBackPressed();
        else{
            startMainActivity();
        }


    }
}
