package com.rider.desinquietos;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.rider.desinquietos.custom.AppController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Guillermo García on 08-08-2016.
 */
public class ItemRecyclerViewAdapter extends RecyclerView.Adapter<ItemViewHolders> {

    private List<ItemObjects> itemList;
    private Context context;
    private MainActivity myActivity;
    private SimpleDraweeView nv;
    private int lastPosition = -1;

    public ItemRecyclerViewAdapter(Context context, List<ItemObjects> itemList,MainActivity myActivity){
        this.itemList = itemList;
        this.context = context;
        this.myActivity = myActivity;
    }
    @Override
    public ItemViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.result_list,null);
        ItemViewHolders rcv = new ItemViewHolders(layoutView,myActivity);
        return rcv;
    }

    @Override
    public void onBindViewHolder(ItemViewHolders holder, int position) {
        nv = null;
        if(!itemList.get(position).getTitle().isEmpty()){
            holder.getItem_title().setText(itemList.get(position).getTitle());
            holder.getItem_marco().setImageResource(itemList.get(position).getMarco());
            nv = (SimpleDraweeView) holder.getItem_fondo();
            Uri _uri = Uri.parse(itemList.get(position).getPhoto());
            nv.setImageURI(_uri);
            holder.getItem_fondo().setVisibility(View.VISIBLE);
            holder.setCodigo(itemList.get(position).getCodigo());
            holder.getItem_subtitle().setText(itemList.get(position).getSubtitle());
            holder.getItem_isla().setText(itemList.get(position).getIsla());
            holder.getItem_beneficio().setText(itemList.get(position).getBeneficio());
            holder.setId_oferta(itemList.get(position).getId_oferta());
            holder.setDescripcion(itemList.get(position).getDescripcion());
            holder.setId_empresa(itemList.get(position).getId_empresa());
            holder.setDomicilio(itemList.get(position).getDomicilio());
            holder.setMunicipio(itemList.get(position).getMunicipio());
            holder.setCp(itemList.get(position).getCp());
            holder.setProvincia(itemList.get(position).getProvincia());
            holder.setTelefono_fijo(itemList.get(position).getTelefono_fijo());
            holder.setTelefono_movil(itemList.get(position).getTelefono_movil());
            holder.setEmail(itemList.get(position).getEmail());
            holder.setLogo(itemList.get(position).getLogo());
            holder.setCategoria(itemList.get(position).getCategoria());
            holder.setSubcategoria(itemList.get(position).getSubcategoria());
            holder.setFoto(itemList.get(position).getPhoto());
            holder.setCompartir(itemList.get(position).getUrl_compartir());
            holder.setWeb(itemList.get(position).getUrl_web());

        }else{
            holder.getItem_title().setText("");
            nv = (SimpleDraweeView)holder.getItem_fondo();
            holder.getItem_marco().setImageResource(itemList.get(position).getPhoto_local());
            holder.getItem_fondo().setVisibility(View.GONE);
            holder.setCodigo(itemList.get(position).getCodigo());
            holder.getItem_subtitle().setText(itemList.get(position).getSubtitle());
            holder.getItem_isla().setText(itemList.get(position).getIsla());
            holder.getItem_beneficio().setText(itemList.get(position).getBeneficio());
            holder.setId_oferta(itemList.get(position).getId_oferta());
            holder.setDescripcion(itemList.get(position).getDescripcion());
            holder.setId_empresa(itemList.get(position).getId_empresa());
            holder.setDomicilio(itemList.get(position).getDomicilio());
            holder.setMunicipio(itemList.get(position).getMunicipio());
            holder.setCp(itemList.get(position).getCp());
            holder.setProvincia(itemList.get(position).getProvincia());
            holder.setTelefono_fijo(itemList.get(position).getTelefono_fijo());
            holder.setTelefono_movil(itemList.get(position).getTelefono_movil());
            holder.setEmail(itemList.get(position).getEmail());
            holder.setLogo(itemList.get(position).getLogo());
            holder.setCategoria(itemList.get(position).getCategoria());
            holder.setSubcategoria(itemList.get(position).getSubcategoria());
            holder.setCompartir(itemList.get(position).getUrl_compartir());
            holder.setWeb(itemList.get(position).getUrl_web());

        }
        setAnimation(holder.getContainer(), position);
    }
    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.up_front_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }else{
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.down_from_top);
            viewToAnimate.startAnimation(animation);
        }
    }


    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public void clear(){
        itemList.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<ItemObjects> listViewItems) {

        itemList.addAll(listViewItems);
        this.notifyDataSetChanged();

    }

}
